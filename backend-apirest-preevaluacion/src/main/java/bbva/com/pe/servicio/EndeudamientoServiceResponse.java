package bbva.com.pe.servicio;

public class EndeudamientoServiceResponse {

	private String codeMsg;
	private String descripcionMsg;

	public EndeudamientoServiceResponse() {
	}

	public String getCodeMsg() {
		return codeMsg;
	}

	public void setCodeMsg(String codeMsg) {
		this.codeMsg = codeMsg;
	}

	public String getDescripcionMsg() {
		return descripcionMsg;
	}

	public void setDescripcionMsg(String descripcionMsg) {
		this.descripcionMsg = descripcionMsg;
	}

}