package bbva.com.pe.servicio.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import bbva.com.pe.modelo.ModeloEndeudamiento;
import bbva.com.pe.repositorio.RepositorioEndeudamiento;
import bbva.com.pe.servicio.EndeudamientoService;

@Service
public class EndeudamientoServiceImpl implements EndeudamientoService {

    @Autowired
    RepositorioEndeudamiento repositorioEndeudamiento;

    @Override
    public List<ModeloEndeudamiento> obtenerEndeudamientos(){
        return this.repositorioEndeudamiento.findAll();
    }
//    public Page<ModeloEndeudamiento> obtenerEndeudamientos(int pagina, int tamanio) {
//        return this.repositorioEndeudamiento.findAll(PageRequest.of(pagina, tamanio));
//    }

    @Override
    public void agregarEndeudamiento(ModeloEndeudamiento endeudamiento) {
        this.repositorioEndeudamiento.insert(endeudamiento);
    }

    @Override
    public void actualizarEndeudamiento(String codigo, ModeloEndeudamiento endeudamiento) {
    	endeudamiento.id = codigo;
        this.repositorioEndeudamiento.save(endeudamiento);
    }

    @Override
    public void eliminarEndeudamiento(String codigo) {
        this.repositorioEndeudamiento.deleteById(codigo);
    }
    
    @Override
    public ModeloEndeudamiento obtenerEndeudamientosById(String codigo) {
    	final Optional<ModeloEndeudamiento> resp = this.repositorioEndeudamiento.findById(codigo);
    	return resp.isPresent() ? resp.get() : null;
    	//return this.repositorioEndeudamiento.findById(codigo).isPresent();
    }

    @Override
    public List<ModeloEndeudamiento> obtenerEndeudamientosPorDNI(String tipoDocumento, String documento) {
        return this.repositorioEndeudamiento.obtenerEndeudamientosPorDNI(tipoDocumento,documento);
    }


}