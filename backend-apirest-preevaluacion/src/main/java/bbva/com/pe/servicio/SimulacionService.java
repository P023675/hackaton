package bbva.com.pe.servicio;

import bbva.com.pe.modelo.ModeloEndeudamiento;
import bbva.com.pe.modelo.ModeloSimulacion;
import org.springframework.data.domain.Page;

import java.util.List;


public interface SimulacionService {

    //public Page<ModeloSimulacion> obtenerSimulaciones(int pagina, int tamanio);
    public List<ModeloSimulacion> obtenerSimulaciones();

    public void agregarSimulacion(ModeloSimulacion s);

    public void actualizarSimulacion(String id, ModeloSimulacion s);

    public void eliminarSimulacion(String id);

    public ModeloSimulacion obtenerSimulacion(String id);

    public List<ModeloSimulacion> obtenerSimulacionesPorDNI(String tipoDocumento, String documento);

}