package bbva.com.pe.servicio.imp;

import bbva.com.pe.modelo.ModeloSimulacion;
import bbva.com.pe.repositorio.RepositorioSimulacion;
import bbva.com.pe.servicio.SimulacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import bbva.com.pe.modelo.ModeloEndeudamiento;
import bbva.com.pe.repositorio.RepositorioEndeudamiento;
import bbva.com.pe.servicio.EndeudamientoService;

import java.util.List;
import java.util.Optional;

@Service
public class SimulacionServiceImpl implements SimulacionService {

    @Autowired
    RepositorioSimulacion repositorioSimulacion;

//    @Override
//    public Page<ModeloSimulacion> obtenerSimulaciones(int pagina, int tamanio) {
//        return this.repositorioSimulacion.findAll(PageRequest.of(pagina, tamanio));
//    }

    @Override
    public List<ModeloSimulacion> obtenerSimulaciones() {
        return this.repositorioSimulacion.findAll();
    }

    @Override
    public void agregarSimulacion(ModeloSimulacion s) {
        this.repositorioSimulacion.insert(s);
    }

    @Override
    public void actualizarSimulacion(String id, ModeloSimulacion s) {
        s.id = id;
        this.repositorioSimulacion.save(s);
    }

    @Override
    public void eliminarSimulacion(String id) {
        this.repositorioSimulacion.deleteById(id);
    }

    @Override
    public ModeloSimulacion obtenerSimulacion(String id) {
        final Optional<ModeloSimulacion> resp = this.repositorioSimulacion.findById(id);
        return resp.isPresent() ? resp.get() : null;
    }

    @Override
    public List<ModeloSimulacion> obtenerSimulacionesPorDNI(String tipoDocumento, String documento) {
        return this.repositorioSimulacion.obtenerSimulacionesPorDNI(tipoDocumento,documento);
    }

}