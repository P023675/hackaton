package bbva.com.pe.servicio;

import bbva.com.pe.modelo.ModeloEndeudamiento;
import org.springframework.data.domain.Page;

import java.util.List;


public interface EndeudamientoService {

    //public Page<ModeloEndeudamiento> obtenerEndeudamientos(int pagina, int tamanio);
    public List<ModeloEndeudamiento> obtenerEndeudamientos();

    public void agregarEndeudamiento(ModeloEndeudamiento endeudamiento);

    public void actualizarEndeudamiento(String codigo, ModeloEndeudamiento endeudamiento);

    public void eliminarEndeudamiento(String codigo);
    
    public ModeloEndeudamiento obtenerEndeudamientosById(String codigo);

    public List<ModeloEndeudamiento> obtenerEndeudamientosPorDNI(String tipoDocumento, String documento);
    
}