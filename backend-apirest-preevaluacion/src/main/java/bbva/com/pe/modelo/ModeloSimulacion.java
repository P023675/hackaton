package bbva.com.pe.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "simulaciones")
public class ModeloSimulacion {

    @Id
    public String id;
    public String tipoDocumento;
    public String documento;
    public String fecha;
    public Double ingresoReal;

    public Double cuotaEstimada;//calculado: sumatoria de endeudamiento.cuotaEstimada
    public Double cuotaReal;//calculado: sumatoria de endeudamiento.cuotaReal
    public Double porcentajeDeudaEstimada;//calculado: ingresoReal / cuotaEstimada
    public Double porcentajeDeudaReal;//calculado: ingresoReal / cuotaReal
    public Double cuotaDiferencial;//calculado: ingresoReal * (50% - porcentajeDeudaReal)

    @JsonIgnore
    public List<String> idsEndeudamientos= new ArrayList<>();

}