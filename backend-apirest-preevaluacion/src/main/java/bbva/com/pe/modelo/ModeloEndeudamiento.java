package bbva.com.pe.modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Document(collection = "endeudamientos")
public class ModeloEndeudamiento {

    @Id //@JsonProperty(access = Access.READ_ONLY)
    public String id;
    public String tipoDocumento;
    public String documento;
    public String banco;
    public String producto;
    public Double deudaEstimada;
    public Double cuotaEstimada;
    public Double cuotaReal;

}