package bbva.com.pe.configuraciones;

public class RutasApi {

    public static final String BASE                   = "/hackaton2021/v1";
    public static final String ENDEUDAMIENTOS         = "/endeudamientos";
    public static final String ENDEUDAMIENTOS_ID      = "/endeudamientos/{id}";
    public static final String SIMULACIONES           = "/simulaciones";
    public static final String SIMULACIONES_ID        = "/simulaciones/{id}";
    
}
