package bbva.com.pe.repositorio;

import bbva.com.pe.modelo.ModeloEndeudamiento;
import bbva.com.pe.modelo.ModeloSimulacion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioSimulacion extends MongoRepository<ModeloSimulacion,String> {

    @Query("{tipoDocumento:?0,documento:?1}")
    public List<ModeloSimulacion> obtenerSimulacionesPorDNI(String tipoDocumento, String documento);
}
