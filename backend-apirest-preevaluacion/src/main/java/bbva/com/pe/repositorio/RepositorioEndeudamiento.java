package bbva.com.pe.repositorio;

import bbva.com.pe.modelo.ModeloEndeudamiento;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioEndeudamiento extends MongoRepository<ModeloEndeudamiento, String> {

    @Query("{tipoDocumento:?0,documento:?1}")
    public List<ModeloEndeudamiento> obtenerEndeudamientosPorDNI(String tipoDocumento, String documento);
}
