package bbva.com.pe.controlador;

import bbva.com.pe.configuraciones.RutasApi;
import bbva.com.pe.modelo.ModeloEndeudamiento;
import bbva.com.pe.modelo.ModeloSimulacion;
import bbva.com.pe.servicio.EndeudamientoService;
import bbva.com.pe.servicio.SimulacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(RutasApi.BASE + RutasApi.SIMULACIONES_ID + RutasApi.ENDEUDAMIENTOS)
public class ControladorEndeudamientosSimulacion {

    @Autowired
    SimulacionService simulacionService;
    @Autowired
    EndeudamientoService endeudamientoService;

    @GetMapping
    public List<String> obtenerIdsEndeudamientos(@PathVariable(name  = "id") String idSimulacion){
        final ModeloSimulacion s = obtenerSimulacionPorId(idSimulacion);
        return s.idsEndeudamientos;
    }

    public static class EndeudamientoEntrada{
        public String id;
    }

    @DeleteMapping("/{idEndeudamiento}")
    public void eliminarReferenciaEndeudamientoSimulacion(@PathVariable(name = "id")  String idSimulacion,
                                                          @PathVariable String idEndeudamiento){

        final ModeloSimulacion x = obtenerSimulacionPorId(idSimulacion);

        boolean encontrado = false;
        int j = 0;
        for(int i = 0; i< x.idsEndeudamientos.size(); i++){
            if(x.idsEndeudamientos.get(i).equals(idEndeudamiento)){
                j = i;
                encontrado = true;
            }
        }

        if(encontrado){
            x.idsEndeudamientos.remove(j);
            this.simulacionService.actualizarSimulacion(idSimulacion,x);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void agregarReferenciaEndeudamientoSimulacion(@PathVariable (name="id") String idSimulacion,
                                                         @RequestBody EndeudamientoEntrada v){

        final ModeloSimulacion x = obtenerSimulacionPorId(idSimulacion);
        final ModeloEndeudamiento y = this.endeudamientoService.obtenerEndeudamientosById(v.id);

        if(y==null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        for (String id: x.idsEndeudamientos){
            if(id.equals(v.id))
                return; // 200 OK
        }

        x.idsEndeudamientos.add(v.id);
        this.simulacionService.actualizarSimulacion(idSimulacion,x);
    }

    public ModeloSimulacion obtenerSimulacionPorId(String id){
        final ModeloSimulacion s = this.simulacionService.obtenerSimulacion(id);
        if(s==null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return s;
    }

}

