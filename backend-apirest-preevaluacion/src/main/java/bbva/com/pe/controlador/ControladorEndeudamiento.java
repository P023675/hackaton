package bbva.com.pe.controlador;

import bbva.com.pe.configuraciones.RutasApi;
import bbva.com.pe.modelo.ModeloEndeudamiento;
import bbva.com.pe.servicio.EndeudamientoService;
import bbva.com.pe.servicio.EndeudamientoServiceResponse;

import io.swagger.models.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(RutasApi.BASE)
public class ControladorEndeudamiento {

    @Autowired
    EndeudamientoService endeudamientoService;

    @GetMapping(RutasApi.ENDEUDAMIENTOS)
    public List<ModeloEndeudamiento> obtenerEndeudamientos(
            @RequestParam(required = false) String tipoDocumento,
            @RequestParam(required = false) String documento){

        if(tipoDocumento != null && !tipoDocumento.isEmpty() && documento != null && !documento.isEmpty()){
            return this.endeudamientoService.obtenerEndeudamientosPorDNI(tipoDocumento, documento);
        }else{
            return this.endeudamientoService.obtenerEndeudamientos();
        }
    }
//    public Page<ModeloEndeudamiento> obtenerEndeudamientos(
//    		@RequestParam(required = false) Integer pagina,
//    		@RequestParam(required = false) Integer tamanio){
//
//		if (pagina == null || pagina < 0)
//			pagina = 0;
//		if (tamanio == null || tamanio < 20)
//			tamanio = 20;
//		if (tamanio > 100)
//			tamanio = 100;
//
//        return this.endeudamientoService.obtenerEndeudamientos(pagina,tamanio);
//    }

    public static class EndeudamientoEntradaPost{
        public String tipoDocumento;
        public String documento;
        public String banco;
        public String producto;
        public Double deudaEstimada;
        public Double cuotaEstimada;
        public Double cuotaReal;
    }

    // POST : Insertar el endeudamiento
    @PostMapping(RutasApi.ENDEUDAMIENTOS)
    public void agregarEndeudamiento(@RequestBody EndeudamientoEntradaPost endeudamiento) {

        ModeloEndeudamiento m = null;

        m.tipoDocumento = endeudamiento.tipoDocumento;
        m.documento = endeudamiento.documento;
        m.banco = endeudamiento.banco;
        m.producto = endeudamiento.producto;
        m.deudaEstimada = endeudamiento.deudaEstimada;
        m.cuotaEstimada = endeudamiento.cuotaEstimada;
        m.cuotaReal = endeudamiento.cuotaReal;

        this.endeudamientoService.agregarEndeudamiento(m);
    }

    public static class EndeudamientoEntrada{
        public Double cuotaReal;
    }
    
    // GetID
    @GetMapping(RutasApi.ENDEUDAMIENTOS_ID)
    public ResponseEntity<ModeloEndeudamiento> obtenerEndeudamientosById (@PathVariable String id){
        try{
            return ResponseEntity.ok(buscarEndeudamientoPorId(id));
        }catch (ResponseStatusException x){
            return ResponseEntity.notFound().build();
        }
    }

    // PUT
    @PutMapping(RutasApi.ENDEUDAMIENTOS_ID)
    public void actualizarEndeudamiento(@PathVariable String id,
                                        @RequestBody EndeudamientoEntrada p) {

        final ModeloEndeudamiento x = buscarEndeudamientoPorId(id);

        if(p.cuotaReal == null || p.cuotaReal <= 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        x.cuotaReal = p.cuotaReal;

        this.endeudamientoService.actualizarEndeudamiento(id,x);
    }

    //PATCH
    @PatchMapping(RutasApi.ENDEUDAMIENTOS_ID)
    public void modificarEndeudamiento(@PathVariable String id,
                                        @RequestBody EndeudamientoEntrada p){
        final ModeloEndeudamiento x = buscarEndeudamientoPorId(id);

        if(p.cuotaReal != null) {
            if(p.cuotaReal <= 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.cuotaReal = p.cuotaReal;
        }

        this.endeudamientoService.actualizarEndeudamiento(id,x);
    }

    // DELETE
    @DeleteMapping(RutasApi.ENDEUDAMIENTOS_ID)
    public void eliminarEndeudamiento(@PathVariable String id) {
        this.endeudamientoService.eliminarEndeudamiento(id);
    }

    private ModeloEndeudamiento buscarEndeudamientoPorId(String idEndeudamiento){
        final ModeloEndeudamiento p = this.endeudamientoService.obtenerEndeudamientosById(idEndeudamiento);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

}
