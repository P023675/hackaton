package bbva.com.pe.controlador;

import bbva.com.pe.configuraciones.RutasApi;
import bbva.com.pe.modelo.ModeloEndeudamiento;
import bbva.com.pe.modelo.ModeloSimulacion;

import bbva.com.pe.servicio.EndeudamientoService;
import bbva.com.pe.servicio.SimulacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(RutasApi.BASE)
public class ControladorSimulacion {

    @Autowired
    SimulacionService simulacionService;

    @Autowired
    EndeudamientoService endeudamientoService;

    @GetMapping(RutasApi.SIMULACIONES)
    public List<ModeloSimulacion> obtenerSimulaciones(
            @RequestParam(required = false) String tipoDocumento,
            @RequestParam(required = false) String documento){

        if(tipoDocumento != null && !tipoDocumento.isEmpty() && documento != null && !documento.isEmpty()){
            return this.simulacionService.obtenerSimulacionesPorDNI(tipoDocumento, documento);
        }else{
            return this.simulacionService.obtenerSimulaciones();
        }
    }

//    @GetMapping(RutasApi.SIMULACIONES)
//    public Page<ModeloSimulacion> obtenerSimulaciones(
//            @RequestParam(required = false) Integer pagina,
//            @RequestParam(required = false) Integer tamanio) {
//
//        if (pagina == null || pagina < 0)
//            pagina = 0;
//        if (tamanio == null || tamanio < 20)
//            tamanio = 20;
//        if (tamanio > 100)
//            tamanio = 100;
//
//        return this.simulacionService.obtenerSimulaciones(pagina,tamanio);
//    }

    public static class SimulacionEntradaPost{
        public String tipoDocumento;
        public String documento;
        public String fecha;
        public Double ingresoReal;
    }

    // POST : Insertar la simulacion
    @PostMapping(RutasApi.SIMULACIONES)
    public void agregarSimulacion(@RequestBody SimulacionEntradaPost s) {

        ModeloSimulacion m = calcularImportes(s.tipoDocumento, s.documento, s.fecha,s.ingresoReal);

        this.simulacionService.agregarSimulacion(m);
    }

    public static class SimulacionEntrada{
        public String fecha;
        public Double ingresoReal;
    }

    // GetID
    @GetMapping(RutasApi.SIMULACIONES_ID)
    public ResponseEntity<ModeloSimulacion> obtenerSimulacion (@PathVariable String id){
        try{
            return ResponseEntity.ok(buscarSimulacionPorId(id));
        }catch (ResponseStatusException x){
            return ResponseEntity.notFound().build();
        }
    }

    // PUT
    @PutMapping(RutasApi.SIMULACIONES_ID)
    public void actualizarSimulacion(@PathVariable String id,
                                     @RequestBody  SimulacionEntrada s) {

        final ModeloSimulacion x = buscarSimulacionPorId(id);

        if(s.fecha == null || s.fecha.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(s.ingresoReal == null || s.ingresoReal <= 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        ModeloSimulacion m = calcularImportes(x.tipoDocumento, x.documento, s.fecha,s.ingresoReal);

        x.fecha = s.fecha;
        x.ingresoReal = s.ingresoReal;

        x.cuotaEstimada = m.cuotaEstimada;
        x.cuotaReal = m.cuotaReal;
        x.porcentajeDeudaEstimada = m.porcentajeDeudaEstimada;
        x.porcentajeDeudaReal = m.porcentajeDeudaReal;
        x.cuotaDiferencial = m.cuotaDiferencial;
        x.idsEndeudamientos = m.idsEndeudamientos;

        this.simulacionService.actualizarSimulacion(id,x);
    }

    //PATCH
    @PatchMapping(RutasApi.SIMULACIONES_ID)
    public void modificarSimulacion(@PathVariable String id,
                                       @RequestBody SimulacionEntrada p){

        final ModeloSimulacion x = buscarSimulacionPorId(id);

        if(p.fecha != null){
            if(p.fecha.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.fecha = p.fecha;
        }
        if(p.ingresoReal != null) {
            if(p.ingresoReal <= 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.ingresoReal = p.ingresoReal;
        }

        ModeloSimulacion m = calcularImportes(x.tipoDocumento, x.documento, p.fecha,p.ingresoReal);

        x.cuotaEstimada = m.cuotaEstimada;
        x.cuotaReal = m.cuotaReal;
        x.porcentajeDeudaEstimada = m.porcentajeDeudaEstimada;
        x.porcentajeDeudaReal = m.porcentajeDeudaReal;
        x.cuotaDiferencial = m.cuotaDiferencial;
        x.idsEndeudamientos = m.idsEndeudamientos;

        this.simulacionService.actualizarSimulacion(id,x);
    }

    // DELETE
    @DeleteMapping(RutasApi.SIMULACIONES_ID)
    public void eliminarSimulacion(@PathVariable String id) {
        this.simulacionService.eliminarSimulacion(id);
    }


    private ModeloSimulacion buscarSimulacionPorId(String idSimulacion){
        final ModeloSimulacion p = this.simulacionService.obtenerSimulacion(idSimulacion);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    private ModeloSimulacion calcularImportes(String tipoDocumento, String documento,
                                              String fecha, Double ingresoReal){

        Double sumaCuotaEstimada = 0.0;
        Double sumaCuotaReal = 0.0;
        Double porcEstimado = 0.0;
        Double porcReal = 0.0;
        Double diferencial = 0.0;

        ModeloSimulacion m = new ModeloSimulacion();
        List<String> listaIndices = new ArrayList<String>();

        if(tipoDocumento!=null && !tipoDocumento.isEmpty() &&
                documento != null && !documento.isEmpty() &&
                fecha != null && !fecha.isEmpty() &&
                ingresoReal != null && ingresoReal>0.0){

            List<ModeloEndeudamiento> listaEndeudamientos =
                    endeudamientoService.obtenerEndeudamientosPorDNI(tipoDocumento,documento);

            for(int i = 0; i< listaEndeudamientos.size(); i++){
                final ModeloEndeudamiento modelo = listaEndeudamientos.get(i);
                sumaCuotaEstimada = sumaCuotaEstimada + modelo.cuotaEstimada;
                sumaCuotaReal = sumaCuotaReal + modelo.cuotaReal;

                listaIndices.add(modelo.id);
            }

            if (sumaCuotaEstimada == 0.0){
                porcEstimado = 0.0;
            }else{
                porcEstimado = sumaCuotaEstimada / ingresoReal;
            }

            if (sumaCuotaReal == 0.0){
                porcReal = porcEstimado;
            }else{
                porcReal = sumaCuotaReal / ingresoReal;
            }

            diferencial = ingresoReal * (0.5 - porcReal);

        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        m.tipoDocumento = tipoDocumento;
        m.documento = documento;
        m.fecha = fecha;
        m.ingresoReal = ingresoReal;
        m.cuotaEstimada = sumaCuotaEstimada;
        m.cuotaReal = sumaCuotaReal;
        m.porcentajeDeudaEstimada = porcEstimado;
        m.porcentajeDeudaReal = porcReal;
        m.cuotaDiferencial = diferencial;
        m.idsEndeudamientos = listaIndices;

        return m;
    }

}
